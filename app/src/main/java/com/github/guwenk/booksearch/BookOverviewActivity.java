package com.github.guwenk.booksearch;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.guwenk.booksearch.library.GBook;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

public class BookOverviewActivity extends AppCompatActivity {

    ImageView coverImageView;
    TextView titleTextView;
    TextView authorTextView;
    TextView ratingTextView;
    Button openGoogleBooksButton;
    TextView descriptionTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_overview);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        final String bookId = (String) Objects.requireNonNull(intent.getExtras()).get("id");

        coverImageView = (ImageView) findViewById(R.id.bCoverImageView);
        titleTextView = (TextView) findViewById(R.id.bTitleTextView);
        authorTextView = (TextView) findViewById(R.id.bAuthorTextView);
        ratingTextView = (TextView) findViewById(R.id.bRatingTextView);
        openGoogleBooksButton = (Button) findViewById(R.id.bOpenGoogleBooksButton);
        descriptionTextView = (TextView) findViewById(R.id.bDescriptionTextView);

        new Thread(new Runnable() {
            @Override
            public void run() {
                final GBook gBook = new GBook(bookId);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("NULL", gBook.getId() + "");
                        if (gBook.getId() == null) {
                            Toast.makeText(BookOverviewActivity.this, R.string.connection_failed, Toast.LENGTH_SHORT).show();
                            Log.d("NULL", "finish");
                            finish();

                        } else {
                            StringBuilder sb = new StringBuilder();

                            titleTextView.setText(gBook.getTitle());

                            for (String s : gBook.getAuthors()) {
                                sb.append(sb.toString().equals("") ? s : " " + s);
                            }
                            authorTextView.setText(sb.toString());

                            sb = new StringBuilder();
                            for (int i = 0; i < gBook.getRating(); i++) {
                                sb.append(getString(R.string.star));
                            }
                            ratingTextView.setText(sb.toString());

                            openGoogleBooksButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(gBook.getGoogleBooksLink()));
                                    startActivity(browserIntent);
                                }
                            });

                            descriptionTextView.setText(Html.fromHtml(gBook.getDescription()));
                            descriptionTextView.setMovementMethod(new ScrollingMovementMethod());

                            setVisible();
                        }
                    }
                });
                if (gBook.getId() != null && gBook.getImageLink() != null) {
                    Bitmap bitmap_temp = null;
                    HttpURLConnection connection = null;
                    if (!gBook.getImageLink().equals("")) {
                        try {
                            connection = (HttpURLConnection) new URL(gBook.getImageLink()).openConnection();
                            InputStream inputStream = connection.getInputStream();
                            bitmap_temp = BitmapFactory.decodeStream(inputStream);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (connection != null) {
                                connection.disconnect();
                            }
                        }
                    } else {
                        bitmap_temp = BitmapFactory.decodeResource(getResources(), R.drawable.no_cover_thumb);
                    }
                    final Bitmap bitmap = bitmap_temp;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            coverImageView.setImageBitmap(bitmap);
                        }
                    });
                }
            }
        }).start();


    }

    private void setVisible() {
        coverImageView.setVisibility(View.VISIBLE);
        titleTextView.setVisibility(View.VISIBLE);
        authorTextView.setVisibility(View.VISIBLE);
        ratingTextView.setVisibility(View.VISIBLE);
        openGoogleBooksButton.setVisibility(View.VISIBLE);
        descriptionTextView.setVisibility(View.VISIBLE);
        findViewById(R.id.borderline).setVisibility(View.VISIBLE);
        findViewById(R.id.bProgressBar).setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
