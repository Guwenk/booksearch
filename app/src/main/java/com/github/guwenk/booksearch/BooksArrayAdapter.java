package com.github.guwenk.booksearch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.guwenk.booksearch.library.GBookPreview;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BooksArrayAdapter extends ArrayAdapter<GBookPreview> {
    private Map<String, Bitmap> bitmaps = new HashMap<>(); //Кэш картинок

    BooksArrayAdapter(Context context, List<GBookPreview> books) {
        super(context, R.layout.list_item, books);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        GBookPreview gBookPreview = getItem(position);
        ViewHolder viewHolder;

        viewHolder = new ViewHolder();
        LayoutInflater inflater = LayoutInflater.from(getContext());
        convertView = inflater.inflate(R.layout.list_item, parent, false);
        viewHolder.coverImageView = (ImageView) convertView.findViewById(R.id.sCoverImageView);
        viewHolder.titleTextView = (TextView) convertView.findViewById(R.id.sTitleTextView);
        viewHolder.authorTextView = (TextView) convertView.findViewById(R.id.sAuthorTextView);
        viewHolder.ratingTextView = (TextView) convertView.findViewById(R.id.sRatingTextView);


        if (bitmaps.containsKey(gBookPreview.getPreviewImageLink())) { //Проверка кэша
            viewHolder.coverImageView.setImageBitmap(bitmaps.get(gBookPreview.getPreviewImageLink())); //Загрузка картинки из кэша
        } else {
            new LoadImageTask(viewHolder.coverImageView).execute(gBookPreview.getPreviewImageLink()); //Загрузка картинки из интернета
        }
        viewHolder.titleTextView.setText(gBookPreview.getTitle());
        viewHolder.authorTextView.setText(gBookPreview.getAuthor());

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < gBookPreview.getRating(); i++) {
            sb.append(getContext().getString(R.string.star));
        }
        viewHolder.ratingTextView.setText(sb.toString());
        return convertView;
    }

    private static class ViewHolder {
        ImageView coverImageView;
        TextView titleTextView;
        TextView authorTextView;
        TextView ratingTextView;
    }

    private class LoadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        LoadImageTask(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            HttpURLConnection connection = null;

            if (params[0].equals("")) { //при отсутствии картинки ставим дефолтную
                bitmap = BitmapFactory.decodeResource(imageView.getContext().getResources(), R.drawable.no_cover_thumb);
                return bitmap;
            }
            try {
                connection = (HttpURLConnection) new URL(params[0]).openConnection();
                InputStream inputStream = connection.getInputStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
                bitmaps.put(params[0], bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(final Bitmap bitmap) {
            imageView.setImageBitmap(bitmap);
        }
    }
}
