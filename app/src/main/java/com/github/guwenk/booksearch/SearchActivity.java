package com.github.guwenk.booksearch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.github.guwenk.booksearch.library.GBookPreview;
import com.github.guwenk.booksearch.library.GLibrary;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    private ArrayList<GBookPreview> gBookPreviewList = new ArrayList<>();
    private BooksArrayAdapter booksArrayAdapter;
    private ListView gBookPreviewListView;
    private boolean flag_loading = false;
    private GLibrary gLibrary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        gBookPreviewListView = (ListView) findViewById(R.id.booksListView);
        booksArrayAdapter = new BooksArrayAdapter(this, gBookPreviewList);
        gBookPreviewListView.setAdapter(booksArrayAdapter);
        gBookPreviewListView.addFooterView(getLayoutInflater().inflate(R.layout.footer, null));
        gBookPreviewListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("ITEM", position + "");
                Intent intent = new Intent(SearchActivity.this, BookOverviewActivity.class);
                intent.putExtra("id", ((GBookPreview) parent.getAdapter().getItem(position)).getId());
                startActivity(intent);
            }
        });

        search_book(" ");
        additems();

        gBookPreviewListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount + 1 == totalItemCount && totalItemCount != 0) {
                    additems();
                }
            }
        });
    }

    private void search_book(final String q) {
        if (!q.equals("")) {
            gLibrary = new GLibrary(JacksonFactory.getDefaultInstance(), q);
            gBookPreviewList.clear();
            booksArrayAdapter.notifyDataSetChanged();
        }
    }

    private void additems() {
        if (!flag_loading) {
            flag_loading = true;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    gLibrary.loadNewPage();
                    final ArrayList<GBookPreview> books = gLibrary.getBooksPreview();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            gBookPreviewList.addAll(books);
                            booksArrayAdapter.notifyDataSetChanged();
                            flag_loading = false;
                        }
                    });
                }
            }).start();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search_book(query);
                additems();
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                return false;
            }
        });
        return true;
    }
}
