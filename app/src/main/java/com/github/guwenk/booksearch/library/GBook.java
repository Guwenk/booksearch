package com.github.guwenk.booksearch.library;

import android.util.Log;

import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.books.Books;
import com.google.api.services.books.model.Volume;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GBook {
    private String id;
    private String title;
    private String description;
    private int rating;
    private List<String> authors;
    private String imageLink;
    private String googleBooksLink;

    public GBook(String id) {
        try {
            Volume.VolumeInfo info = new Books.Builder(new com.google.api.client.http.javanet.NetHttpTransport(), JacksonFactory.getDefaultInstance(), null)
                    .setApplicationName("BookSearch")
                    .build()
                    .volumes()
                    .get(id)
                    .execute()
                    .getVolumeInfo();

            this.title = info.getTitle();
            this.rating = info.getAverageRating() != null ? info.getAverageRating().intValue() : 0;
            Log.d("RATING", String.valueOf(info.getAverageRating()));
            this.authors = info.getAuthors() != null ? info.getAuthors() : new ArrayList<String>() {
            };
            this.imageLink = info.getImageLinks() != null ? info.getImageLinks().getSmall() : "";
            this.description = info.getDescription() != null ? info.getDescription() : "";
            this.googleBooksLink = info.getInfoLink();
            this.id = id;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getGoogleBooksLink() {
        return googleBooksLink;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public double getRating() {
        return rating;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public String getImageLink() {
        return imageLink;
    }
}
