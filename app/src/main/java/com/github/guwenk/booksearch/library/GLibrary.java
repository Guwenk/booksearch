package com.github.guwenk.booksearch.library;

import com.google.api.client.json.JsonFactory;
import com.google.api.services.books.Books;
import com.google.api.services.books.model.Volume;
import com.google.api.services.books.model.Volumes;

import java.io.IOException;
import java.util.ArrayList;

public class GLibrary {
    private Books books;
    private Volumes volumes;
    private long startIndex;
    private String query;

    public GLibrary(JsonFactory jsonFactory, String query) {
        books = new Books.Builder(new com.google.api.client.http.javanet.NetHttpTransport(), jsonFactory, null)
                .setApplicationName("BookSearch")
                .build();
        this.query = query;
        startIndex = 0;
    }

    public void loadNewPage() {
        while (true) {
            try {
                volumes = books.volumes().list(query).setStartIndex(startIndex).execute();
                startIndex += 10;
                return;
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }

    }

    public ArrayList<GBookPreview> getBooksPreview() {
        ArrayList<GBookPreview> list = new ArrayList<>();
        if (volumes != null)
            for (Volume volume : volumes.getItems()) {
                GBookPreview book = new GBookPreview();
                book.setId(volume.getId());
                book.setTitle(volume.getVolumeInfo().getTitle());
                try {
                    book.setAuthor(volume.getVolumeInfo().getAuthors().get(0));
                } catch (NullPointerException ignored) {
                }
                try {
                    book.setPreviewImageLink(volume.getVolumeInfo().getImageLinks().getSmallThumbnail());
                } catch (NullPointerException ignored) {
                }
                try {
                    book.setRating(volume.getVolumeInfo().getAverageRating());
                } catch (NullPointerException ignored) {
                }
                list.add(book);
            }
        return list;
    }
}
